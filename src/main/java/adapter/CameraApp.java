package adapter;

public interface CameraApp {
    void share();
    void takePhoto();
    void display();
    default void save(){
        System.out.println("Save a file");
    }
    default void edit(){
        System.out.println("Edit a file");
    }
}
