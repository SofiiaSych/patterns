package adapter;

public interface Drone {
    void takeVideo();
    void shareOnCloud();
}
