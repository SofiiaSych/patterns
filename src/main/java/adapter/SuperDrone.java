package adapter;

public class SuperDrone implements Drone{
    @Override
    public void takeVideo() {
        System.out.println("Take a video");
    }

    @Override
    public void shareOnCloud() {
        System.out.println("Share on cloud");
    }
}
