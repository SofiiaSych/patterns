package adapter;

public class App {
    public static void main(String[] args) {
        Drone drone = new SuperDrone();
        CameraApp droneAdapter = new DroneAdapter(drone);
        testCamera(droneAdapter);

    }
    static void testCamera(CameraApp simpleCameraApp){
        simpleCameraApp.display();
        simpleCameraApp.takePhoto();
        simpleCameraApp.edit();
        simpleCameraApp.save();
        simpleCameraApp.share();
    }
}
