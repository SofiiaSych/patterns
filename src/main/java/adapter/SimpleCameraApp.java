package adapter;

public class SimpleCameraApp implements CameraApp {

    public SimpleCameraApp() {
    }

    @Override
    public void share() {
        System.out.println("Share via social media");
    }

    @Override
    public void takePhoto() {
        System.out.println("Take a photo");
    }

    @Override
    public void display() {
        System.out.println("It is a simple camera app");
    }

}
