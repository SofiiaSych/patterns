package adapter;

import jdk.swing.interop.SwingInterOpUtils;

public class DroneAdapter implements CameraApp {

    Drone drone;

    public DroneAdapter(Drone drone) {
        this.drone = drone;
    }

    @Override
    public void share() {
        drone.shareOnCloud();
    }

    @Override
    public void takePhoto() {
        drone.takeVideo();
    }

    @Override
    public void display() {
        System.out.println("It is a drone");
    }
}
