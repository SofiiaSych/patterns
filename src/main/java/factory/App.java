package factory;

import factory.enums.PizzaType;
import factory.factory.Pizzeria;
import factory.factory.impl.DniproPizzeria;
import factory.factory.impl.KyivPizzeria;
import factory.factory.impl.LvivPizzeria;
import factory.models.Pizza;

public class App {
    public static void main(String[] args) {
        Pizzeria lvivPizzeria = new LvivPizzeria();
        Pizza lvivPizzaCalm = lvivPizzeria.orderPizza(PizzaType.CALM);
        lvivPizzaCalm.readyToEat();
        Pizza lvivPizzaCheese = lvivPizzeria.orderPizza(PizzaType.CHEESE);
        lvivPizzaCheese.readyToEat();

        Pizzeria kyivPizzeria = new KyivPizzeria();
        Pizza kyivPizzaPapperoni = kyivPizzeria.orderPizza(PizzaType.PEPPERONI);
        kyivPizzaPapperoni.readyToEat();
        Pizza kyivPizzaVeggie = kyivPizzeria.orderPizza(PizzaType.VEGGIE);
        kyivPizzaVeggie.readyToEat();

        Pizzeria dniproPizzeria = new DniproPizzeria();
        Pizza dniproPizzaCheese = dniproPizzeria.orderPizza(PizzaType.CHEESE);
        dniproPizzaCheese.readyToEat();
    }
}
