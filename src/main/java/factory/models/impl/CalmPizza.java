package factory.models.impl;

import factory.enums.DoughCrust;
import factory.enums.Sauce;
import factory.models.IPizza;
import factory.models.Pizza;

import java.util.List;

public class CalmPizza extends Pizza implements IPizza {

    public CalmPizza(String name, DoughCrust doughCrust, Sauce sauce, List<String> toppings){
        super(name, doughCrust, sauce, toppings);
    }

    @Override
    public void cut() {
        System.out.println("Cutting the pizza into square slices");
    }
}