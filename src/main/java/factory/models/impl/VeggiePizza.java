package factory.models.impl;

import factory.enums.DoughCrust;
import factory.enums.Sauce;
import factory.models.IPizza;
import factory.models.Pizza;

import java.util.List;

public class VeggiePizza extends Pizza implements IPizza {

    public VeggiePizza(String name, DoughCrust doughCrust, Sauce sauce, List<String> toppings){
        super(name, doughCrust, sauce, toppings);
    }

    @Override
    public void bake() {
        System.out.println("Bake for 15 minutes at 180'C");
    }

    @Override
    public void cut() {
        System.out.println("Cutting the pizza into square slices");
    }
}
