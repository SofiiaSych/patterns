package factory.models.impl;

import factory.enums.DoughCrust;
import factory.enums.Sauce;
import factory.models.IPizza;
import factory.models.Pizza;

import java.util.List;

public class PepperoniPizza extends Pizza implements IPizza {

    public PepperoniPizza(String name, DoughCrust doughCrust, Sauce sauce, List<String> toppings){
        super(name, doughCrust, sauce, toppings);
    }
}
