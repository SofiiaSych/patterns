package factory.models.impl;

import factory.enums.DoughCrust;
import factory.enums.Sauce;
import factory.models.IPizza;
import factory.models.Pizza;

import java.util.List;

public class CheesePizza extends Pizza implements IPizza {

    public CheesePizza(String name, DoughCrust doughCrust, Sauce sauce, List<String> toppings){
        super(name, doughCrust, sauce, toppings);
    }

    @Override
    public void bake() {
        System.out.println("Bake for 10 minutes at 200'C");
    }
}
