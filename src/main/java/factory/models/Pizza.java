package factory.models;

import factory.enums.DoughCrust;
import factory.enums.Sauce;

import java.util.List;

public abstract class Pizza implements IPizza {

    protected DoughCrust doughCrust;
    protected Sauce sauce;
    protected String name;
    protected List<String> toppings;

    public Pizza(String name, DoughCrust doughCrust, Sauce sauce, List<String> toppings) {
        this.name = name;
        this.doughCrust = doughCrust;
        this.sauce = sauce;
        this.toppings = toppings;
    }

    @Override
    public void prepare() {
        System.out.println(name + " is preparing");
        System.out.println("Tossing dough with " + doughCrust.value + " crust");
        System.out.println("Adding sauce " + sauce.value);
        String sToppings = "Adding toppings: ";
        for (String topping: toppings) sToppings += topping + ", ";
        System.out.println(sToppings);
    }

    @Override
    public void bake() {
        System.out.println("Bake for 15 minutes at 250'C");
    }

    @Override
    public void cut() {
        System.out.println("Cutting the pizza into diagonal slices");
    }

    @Override
    public void box() {
        System.out.println("Place pizza in official PizzaStore box");
    }

    @Override
    public void readyToEat() {
        System.out.println("Pizza " + name + " is ready to eat\n");
    }
}
