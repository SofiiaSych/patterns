package factory.enums;

public enum Sauce {
    MARINARA("Marinara"),
    PLUM_TOMATO("Plum Tomato"),
    PESTO("Pesto");

    public String value;

    private Sauce(String value){
        this.value = value;
    }
}
