package factory.enums;

public enum PizzaType {
    CHEESE("Cheese pizza"),
    VEGGIE("Veggie pizza"),
    CALM("Calm pizza"),
    PEPPERONI("Pepperoni pizza");

    private String value;

    private PizzaType(String value){
        this.value = value;
    }
}
