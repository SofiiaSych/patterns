package factory.enums;

public enum DoughCrust {
    THICK("thick"),
    THIN("thin");

    public String value;

    private DoughCrust(String value){
        this.value = value;
    }
}