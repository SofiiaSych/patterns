package factory.factory;

import factory.enums.PizzaType;
import factory.models.Pizza;

public abstract class Pizzeria {
    protected abstract Pizza createPizza(PizzaType pizzaType);

    public  Pizza orderPizza(PizzaType pizzaType){
        Pizza pizza = createPizza(pizzaType);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();;

        return pizza;
    }
}

