package factory.factory.impl;

import factory.enums.DoughCrust;
import factory.enums.PizzaType;
import factory.enums.Sauce;
import factory.factory.Pizzeria;
import factory.models.Pizza;
import factory.models.impl.CalmPizza;
import factory.models.impl.CheesePizza;
import factory.models.impl.PepperoniPizza;
import factory.models.impl.VeggiePizza;

import java.util.Arrays;
import java.util.List;

public class KyivPizzeria extends Pizzeria {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        Pizza pizza = null;
        List<String> toppings;

        switch (pizzaType) {
            case CHEESE:
                toppings = Arrays.asList("mozzarella", "gorgonzola", "parmesan", "emmental");
                pizza = new CheesePizza("Cheese pizza", DoughCrust.THIN, Sauce.MARINARA, toppings);
                break;
            case VEGGIE:
                toppings = Arrays.asList("broccoli", "carrots", "celery", "tomatoes");
                pizza = new VeggiePizza("Veggie pizza", DoughCrust.THICK, Sauce.PESTO, toppings);
                break;
            case CALM:
                toppings = Arrays.asList("double mozzarella", "clams", "parmesan", "romano salad", "oregano");
                pizza = new CalmPizza("Calm pizza", DoughCrust.THIN, Sauce.PLUM_TOMATO, toppings);
                break;
            case PEPPERONI:
                toppings = Arrays.asList("mozzarella", "pepperoni", "tomatoes");
                pizza = new PepperoniPizza("Pepperoni pizza", DoughCrust.THIN, Sauce.PLUM_TOMATO, toppings);
                break;
        }
        return pizza;
    }
}

