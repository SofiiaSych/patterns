package strategy.strategies;

import strategy.ShareStrategy;

public class TXTStrategy implements ShareStrategy {
    @Override
    public void share() {
        System.out.println("Share via txt");
    }
}
