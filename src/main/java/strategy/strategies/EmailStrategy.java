package strategy.strategies;

import strategy.ShareStrategy;

public class EmailStrategy implements ShareStrategy {
    @Override
    public void share() {
        System.out.println("Share via email");
    }
}
