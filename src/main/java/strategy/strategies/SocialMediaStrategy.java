package strategy.strategies;

import strategy.ShareStrategy;

public class SocialMediaStrategy implements ShareStrategy {
    @Override
    public void share() {
        System.out.println("Share via social media");
    }
}
