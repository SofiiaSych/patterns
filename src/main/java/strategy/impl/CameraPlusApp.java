package strategy.impl;

import strategy.CameraApp;
import strategy.strategies.SocialMediaStrategy;

public class CameraPlusApp extends CameraApp {

    public CameraPlusApp() {
        shareStrategy = new SocialMediaStrategy();
    }

    @Override
    protected void edit() {
        System.out.println("You can crop the photo, change the exposition, shades, lights, add some filters");
    }

    @Override
    protected void display() {
        System.out.println("It is a camera plus app");
    }
}
