package strategy.impl;

import strategy.CameraApp;
import strategy.strategies.EmailStrategy;

public class SimpleCameraApp extends CameraApp {

    public SimpleCameraApp() {
        shareStrategy = new EmailStrategy();
    }

    @Override
    protected void edit() {
        System.out.println("You can crop the photo, change the exposition, shades, lights");
    }

    @Override
    protected void display() {
        System.out.println("It is a simple camera app");
    }
}
