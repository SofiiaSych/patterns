package strategy;

public interface ShareStrategy {
    void share();
}
