package strategy;

public abstract class CameraApp {
    public ShareStrategy shareStrategy;
    protected abstract void edit();
    protected abstract void display();
    public CameraApp() {
    }
    public void performShare(){
        shareStrategy.share();
    }
    public void takePhoto(){
        System.out.println("Take a photo");
    }
    public void save(){
        System.out.println("Save a photo");
    }
}
