package strategy;

import strategy.impl.CameraPlusApp;
import strategy.impl.SimpleCameraApp;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        CameraApp simpleCameraApp = new SimpleCameraApp();
        CameraApp cameraPlusApp = new CameraPlusApp();

        System.out.println("Chose the camera app(1-simple 2-plus)");
        Scanner scanner = new Scanner(System.in);
        int camera = scanner.nextInt();
        switch (camera){
            case 1 -> {
                simpleCameraApp.display();
                simpleCameraApp.takePhoto();
                simpleCameraApp.edit();
                simpleCameraApp.save();
                simpleCameraApp.performShare();
            }
            case 2 -> {
                cameraPlusApp.display();
                cameraPlusApp.takePhoto();
                cameraPlusApp.edit();
                cameraPlusApp.save();
                cameraPlusApp.performShare();
            }
        }
    }
}
